@echo off
cls

rem Recreate virtual environment
echo == VIRTUAL ENVIRONMENT ==
if exist .venv\ (
    echo * Found previous, removing
    rmdir /s /q .venv
)
echo * Creating new virtual environment
virtualenv .venv
echo * Activating virtual environment
call .venv\Scripts\activate


rem Set up python environment (download all requirements)
echo(
echo == PYTHON ==
echo * Installing requirements
pip install -r requirements.txt


rem Set up database
call setup-db.bat


rem Aliases
echo(
echo == ALIASES ==
echo * 'serrun' to start Django server
doskey serrun=python manage.py runserver
echo * 'upall' to remake python virtual environment from scratch
doskey upall=setup.bat
echo * 'updb' to update migrations
doskey updb=setup-db.bat
echo * 'upreq' to remake requirements file
doskey upreq=pip freeze $g requirements.txt
