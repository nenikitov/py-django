@echo off

rem Get the password
echo(
echo == DJANGO ==
if defined DJANGO_PWD (
    echo * Using old password
) else (
    set /p DJANGO_PWD=* Password: 
)
echo * Migrating models
python manage.py makemigrations
python manage.py migrate
