from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('about', views.AboutView.as_view(), name='about'),
    path('about/contact', views.ContactView.as_view(), name='contact'),
    path('list/<int:list_id>', views.todo_list, name='list'),
    path('list/<int:list_id>/item/<int:item_id>', views.todo_item, name='item'),
]
