from re import template
from django.shortcuts import render
from django.shortcuts import HttpResponse
from django.template import loader
from django.views import View
from django.views.generic.base import TemplateView
from .models import TodoList, TodoListItem
from todo_app import models

# Create your views here.
def index(request):
    db_todo_lists = TodoList.objects.order_by('-title')
    for todo_list in db_todo_lists:
        todo_list.items = TodoListItem.objects.filter(todo_list=todo_list)

    template = loader.get_template('todo_app/home.html')
    context = {
        'todo_lists': db_todo_lists
    }
    return HttpResponse(template.render(context, request))


class AboutView(View):
    def get(self, request):
        return HttpResponse('<h1>About Page</h1>')


class ContactView(TemplateView):
    template_name = 'about/contact.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['contact_num'] = '514-555-1234'

        return context


def todo_list(request, list_id):
    try:
        db_todo_list = models.TodoList.objects.get(id=list_id)
        db_todo_items = TodoListItem.objects.filter(todo_list=db_todo_list)

        template = loader.get_template('todo_app/todo_list.html')
        context = {
            'todo_list': db_todo_list,
            'todo_items': db_todo_items
        }
        return HttpResponse(template.render(context, request))
    except:
        return HttpResponse('No list with this id', request)


def todo_item(request, list_id, item_id):
    try:
        db_todo_list = models.TodoList.objects.get(id=list_id)
        db_todo_item = TodoListItem.objects.get(id=item_id, todo_list=db_todo_list)

        template = loader.get_template('todo_app/todo_item.html')
        context = {
            'todo_item': db_todo_item
        }
        return HttpResponse(template.render(context, request))
    except:
        return HttpResponse('No item with this id', request)