from datetime import date
from todo_app.models import *

def add_if_not_exists(model, **args):
    filter = {k: v for (k, v) in args.items() if not isinstance(v, date)}
    in_db = model.objects.filter(**filter)
    if in_db.count() == 0:
        new = model(**args)
        new.save()
        return new
    else:
        return in_db[0]


#region Persons
john_smith = add_if_not_exists(Person, first_name='John', last_name='Smith', creation_date=date(2022, 3, 1))
jane_smith = add_if_not_exists(Person, first_name='Jane', last_name='Smith', creation_date=date(2022, 3, 15))
bob_jones  = add_if_not_exists(Person, first_name='Bob', last_name='Jones', creation_date=date(2022, 4, 1))
#endregion

#region John Smith todo lists
john_smith_our_vacation = add_if_not_exists(TodoList, title='Our vacation', person=john_smith)
john_smith_our_vacation_share_jane_smith = add_if_not_exists(TodoListSharedPerson, todo_list=john_smith_our_vacation, person=jane_smith, share_date=date(2022, 3, 17))
john_smith_my_chores = add_if_not_exists(TodoList, title='My Chores', person=john_smith)
#endregion

#region Jane Smith todo lists
jane_smith_my_chores = add_if_not_exists(TodoList, title='My Chores', person=jane_smith)
jane_smith_my_chores_share_john_smith = add_if_not_exists(TodoListSharedPerson, todo_list=jane_smith_my_chores, person=john_smith, share_date=date(2022, 3, 15))
jane_smith_house_maintenance = add_if_not_exists(TodoList, title='House maintenance', person=jane_smith)
jane_smith_house_maintenance_share_bob_jones = add_if_not_exists(TodoListSharedPerson, todo_list=jane_smith_house_maintenance, person=bob_jones, share_date=date(2022, 4, 5))
jane_smith_my_relaxation_plan = add_if_not_exists(TodoList, title='My relaxation plan', person=jane_smith)
#endregion

#region Bob Jones todo lists
bob_jones_work_stuff = add_if_not_exists(TodoList, title='Work stuff', person=bob_jones)
#endregion
