from django.contrib import admin
from todo_app.models import *

# Register your models here.
admin.site.register(TodoList)
admin.site.register(TodoListItem)
admin.site.register(Person)
admin.site.register(TodoListSharedPerson)
