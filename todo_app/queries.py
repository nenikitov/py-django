from multiprocessing.shared_memory import ShareableList
from todo_app.models import *

def query_person(first_name, last_name):
    if person := Person.objects.filter(first_name=first_name, last_name=last_name)[0]:
        owned = TodoList.objects.filter(person=person)
        shared = [ s.todo_list for s in TodoListSharedPerson.objects.filter(person=person) ]
        return list(set(owned) | set(shared))
    else:
        return []

print(query_person('John', 'Smith'))
print(query_person('Jane', 'Smith'))
print(query_person('Bob', 'Jones'))