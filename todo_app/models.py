from asyncio import constants
from django.db import models
from django.forms import ValidationError
from django.utils import timezone

# Create your models here
class Person(models.Model):
    first_name    = models.CharField(max_length=64)
    last_name     = models.CharField(max_length=64)
    creation_date = models.DateField(auto_now_add=True)

    def __str__(self) -> str:
        return f'{self.first_name} {self.last_name}'


class TodoList(models.Model):
    title  = models.CharField(max_length=128)
    person = models.ForeignKey(Person, on_delete=models.CASCADE, null=True)

    def __str__(self) -> str:
        return f'{self.title} ({self.person or "NO OWNER"})'


class TodoListSharedPerson(models.Model):
    todo_list  = models.ForeignKey(TodoList, on_delete=models.CASCADE)
    person     = models.ForeignKey(Person, on_delete=models.CASCADE)
    share_date = models.DateField(auto_now_add=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=[ 'todo_list', 'person' ],
                name='Share unique persons only'
            )
        ]

    def __str__(self) -> str:
        return f'{self.todo_list} - {self.person} ({self.share_date})'


class TodoListItem(models.Model):
    def next_week():
        return timezone.now().date() + timezone.timedelta(days=7)

    def due_date_validator(due_date):
        if due_date <= timezone.now().date():
            raise ValidationError('Due date can\'t be before today')

    title         = models.CharField(max_length=128)
    description   = models.TextField(blank=True, null=True)
    creation_date = models.DateField(auto_now_add=True)
    due_date      = models.DateField(default=next_week, validators=[due_date_validator])
    todo_list     = models.ForeignKey(TodoList, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return f'{self.todo_list} - {self.title} (created: {self.creation_date}, due: {self.due_date})'
